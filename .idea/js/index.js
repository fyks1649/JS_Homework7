'use strict'

function excludeBy(arr1, arr2, parametr) {
    let arrResult = []; // скопировали первый список людей в результирующий список
    arr1.forEach(function(human, index) {
        let isCoincidence = false;
        for (let i = 0; i < arr2.length; i++) {
            if(human[parametr] == arr2[i][parametr]) { // если N-й человек из первого списка оказался во втором
                isCoincidence = true; // удаляем его из первого списка
                break; // нет смысла дальше перебирать, баба Глаша попалась
            }
        }
        if(!isCoincidence) {
            arrResult.push(human);
        }
    });
//    arrResult.splice(0, 1);
//    arrResult.splice(0, 1);
    console.log(arrResult);

}

const peopleList = [{
    name: "Ivan",
    surname: "Ivanov",
    gender: "male",
    age: 30
},
    {
        name: "Anna",
        surname: "Ivanova",
        gender: "female",
        age: 22
    },
    {
        name: "Anna",
        surname: "Shtil",
        gender: "female",
        age: 22
    },
    {
        name: "Anna",
        surname: "Petrovna",
        gender: "female",
        age: 22
    }]

const excluded = [{
    name: "Vano",
    surname: "Ivanov",
    gender: "male",
    age: 30
},
    {
        name: "Hannah",
        surname: "Ivanova",
        gender: "female",
        age: 22
    }]

excludeBy(peopleList, excluded, 'name');